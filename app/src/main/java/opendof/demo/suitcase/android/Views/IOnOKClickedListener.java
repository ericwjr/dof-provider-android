package opendof.demo.suitcase.android.Views;

/**
 * Created by zdusatko on 7/31/2015.
 */
public interface IOnOKClickedListener
{
    public void onOKClicked(String host, int port, String oid, String domain, String identity, String password);
}
