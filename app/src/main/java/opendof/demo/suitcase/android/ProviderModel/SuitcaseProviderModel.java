package opendof.demo.suitcase.android.ProviderModel;

import org.opendof.core.oal.DOF;
import org.opendof.core.oal.DOFApplicationErrorException;
import org.opendof.core.oal.DOFException;
import org.opendof.core.oal.DOFInterface.Event;
import org.opendof.core.oal.DOFInterface.Method;
import org.opendof.core.oal.DOFInterface.Property;
import org.opendof.core.oal.DOFInterfaceID;
import org.opendof.core.oal.DOFNotFoundException;
import org.opendof.core.oal.DOFObject;
import org.opendof.core.oal.DOFObjectID;
import org.opendof.core.oal.DOFOperation;
import org.opendof.core.oal.DOFOperation.Provide;
import org.opendof.core.oal.DOFRequest.Get;
import org.opendof.core.oal.DOFRequest.Invoke;
import org.opendof.core.oal.DOFRequest.Register;
import org.opendof.core.oal.DOFRequest.Session;
import org.opendof.core.oal.DOFRequest.Set;
import org.opendof.core.oal.DOFRequest.Subscribe;
import org.opendof.core.oal.DOFSystem;
import org.opendof.core.oal.DOFValue;
import org.opendof.core.oal.value.DOFBoolean;
import org.opendof.core.oal.value.DOFFloat32;
import org.opendof.core.oal.value.DOFFloat64;
import org.opendof.core.oal.value.DOFStructure;

import java.util.Arrays;
import java.util.List;

import opendof.demo.suitcase.common.GPSLocationInterface;
import opendof.demo.suitcase.common.ImpactInterface;
import opendof.demo.suitcase.common.LockableInterface;
import opendof.demo.suitcase.common.WeightInterface;

/**
 * <h1>Suitcase Provider Model</h1>
 * This implements model part in MVC pattern. It uses SuitcaseDataStruct object for updating data and has event listener
 * when the data changes. This model also implements DOF provider.
 * <p></p>.
 *
 * @author zdusatko - upgraded
 * @since 01/13/2015
 */
public class SuitcaseProviderModel implements ISuitcaseProviderDemoMBean, DOFObject.Provider
{
    private final Object _monitor = new Object(); // Guards _system, object, and provide ops.
    private DOFObject _providerObject = null;
    private DOFSystem _dofSystem = null;

    private Provide _weightProvide = null;
    private Provide _lockProvide = null;
    private Provide _gpsProvide = null;
    private Provide _impactProvide = null;
    //private final Logger _logger = LoggerFactory.getLogger(SuitcaseProviderModel.class);
    private boolean _isLocationKnown = false;
    private SuitcaseDataStruct _data;
    private static SuitcaseProviderModel _instance = null;

    private static final float DEFAULT_THRESHOLD = 1.1f; // in Gs

    public SuitcaseDataStruct.SuitcaseState getState()
    {
        return _data.getState();
    }

    public void setState(SuitcaseDataStruct.SuitcaseState state)
    {
        _data.setState(state);
    }

    private IOnSuitcaseChangedListener _onSuitcaseChangedListener = null;
    public void setOnSuitcaseChangedListener(IOnSuitcaseChangedListener onSuitcaseChangedListener)
    {
        _onSuitcaseChangedListener = onSuitcaseChangedListener;
    }

    // Suitcase provider is a singleton
    public static SuitcaseProviderModel getInstance()
    {
        if (_instance == null)
            synchronized (SuitcaseProviderModel.class) {
                _instance = new SuitcaseProviderModel();
            }
        return _instance;
    }

    // CONSTRUCTOR
    private SuitcaseProviderModel()
    {
        _data = new SuitcaseDataStruct();
        // default is red not connected
        setState(SuitcaseDataStruct.SuitcaseState.RED);
        // set impact threshold
        setThreshold(DEFAULT_THRESHOLD); // this is in Gs
    }

    public void startProvide(String oid, DOFSystem dofSystem)
    {
        try {
            synchronized (_monitor) {
                _dofSystem = dofSystem;
                DOFObjectID dofObjectID = DOFObjectID.create(oid);
                _providerObject = _dofSystem.createObject(dofObjectID);
                _weightProvide = _providerObject.beginProvide(WeightInterface.iface, DOF.TIMEOUT_NEVER, SuitcaseProviderModel.this, null);
                _lockProvide = _providerObject.beginProvide(LockableInterface.iface, DOF.TIMEOUT_NEVER, SuitcaseProviderModel.this, null);
                _gpsProvide = _providerObject.beginProvide(GPSLocationInterface.iface, DOF.TIMEOUT_NEVER, SuitcaseProviderModel.this, null);
                _impactProvide = _providerObject.beginProvide(ImpactInterface.iface, DOF.TIMEOUT_NEVER, SuitcaseProviderModel.this, null);
                _data.setDofSystem(_dofSystem);

                if (_onSuitcaseChangedListener != null)
                    _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.PROVIDE, _data); //_onSuitcaseProvideBeginListener.onSuitcaseProvideBegin();
            }

            //_logger.info("Provide started.");
        }
        catch (Exception e) {
            //_logger.error("Suitcase failed to startProvide providing: " + e, e);
            _data.setException(e);
            if (_onSuitcaseChangedListener != null) {
                _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.EXCEPTION, _data); //_onProviderExceptionListener.onProviderException(_dofSystem);
            }

        }
    }

    public void destroy()
    {
        if (_providerObject != null) {
            _providerObject.destroy();
            _providerObject = null;
        }

        // also destroyConnection all listeners
        _onSuitcaseChangedListener = null;
    }

    // ********* ISuitcaseProviderDemoMBean ********
    @Override
    public void lock()
    {
        if (!_data.isLocked()) {
            //_logger.info("Suitcase locked.");
            _data.setLocked(true);

            if (_onSuitcaseChangedListener != null)
                _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.LOCK, _data);

            synchronized (_monitor) {
                if (_providerObject != null)
                    _providerObject.changed(LockableInterface.propertyIsLocked);
            }
        }
    }

    @Override
    public void unlock()
    {
        if (_data.isLocked()) {
            //_logger.info("Suitcase unlocked.");
            _data.setLocked(false);

            if (_onSuitcaseChangedListener != null)
                _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.LOCK, _data);

            synchronized (_monitor) {
                if (_providerObject != null)
                    _providerObject.changed(LockableInterface.propertyIsLocked);
            }
        }
    }

    @Override
    public boolean isLocked()
    {
        return _data.isLocked();
    }

    @Override
    public void setLocation(double longitude, double latitude, double elevation)
    {
        //_logger.info("Location set to " + longitude + ", " + latitude + ", " + elevation);
        _data.setLongitude(longitude);
        _data.setLatitude(latitude);
        _data.setElevation(elevation);
        _isLocationKnown = true;

        if (_onSuitcaseChangedListener != null)
            _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.GPS, _data);

        synchronized (_monitor) {
            if ((_gpsProvide == null || _gpsProvide.isCancelled()) && _providerObject != null)
                _gpsProvide = _providerObject.beginProvide(GPSLocationInterface.iface, DOF.TIMEOUT_NEVER, SuitcaseProviderModel.this, null);
            if (_providerObject != null)
                _providerObject.changed(GPSLocationInterface.propertyLocation);
        }
    }

    @Override
    public void setLocationUnknown()
    {
        _isLocationKnown = false;
        synchronized (_monitor) {
            if (_gpsProvide != null)
                _gpsProvide.cancel();
        }
    }

    @Override
    public double getLongitude()
    {
        return _data.getLongitude();
    }

    @Override
    public double getLatitude()
    {
        return _data.getLatitude();
    }

    @Override
    public double getElevation()
    {
        return _data.getElevation();
    }

    @Override
    public void setThreshold(double threshold)
    {
        _data.setThreshold(threshold);

        if (_onSuitcaseChangedListener != null)
            _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.THRESHOLD, _data);

        synchronized (_monitor) {
            if (_providerObject != null)
                _providerObject.changed(ImpactInterface.propertyThreshold);
        }
    }

    @Override
    public double getThreshold()
    {
        return _data.getThreshold();
    }

    @Override
    public void impactEvent(float x, float y, float z) throws Exception
    {
        float magnitude = (float) Math.sqrt(x * x + y * y + z * z);

        float dx = x / magnitude;
        float dy = y / magnitude;
        float dz = z / magnitude;

        impactEvent(dx, dy, dz, magnitude);
    }

    @Override
    public void impactEvent(float dx, float dy, float dz, double magnitude) throws Exception
    {
        if (magnitude >= _data.getThreshold()) {
            //_logger.info("Impact event: " + magnitude);

            // update data model
            _data.setImpactX(dx);
            _data.setImpactY(dy);
            _data.setImpactZ(dz);
            _data.setImpactMagnitude(magnitude);

            if (_onSuitcaseChangedListener != null)
                _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.IMPACT, _data);

            final DOFValue location;

            if (_isLocationKnown) {
                DOFValue[] locfields = new DOFValue[3];
                locfields[0] = new DOFFloat64(_data.getLatitude());
                locfields[1] = new DOFFloat64(_data.getLongitude());
                locfields[2] = new DOFFloat64(_data.getElevation());
                location = new DOFStructure(ImpactInterface.Location, locfields);
            }
            else
                location = null;

            DOFValue[] forceFields = new DOFValue[4];
            forceFields[0] = new DOFFloat32(dx);
            forceFields[1] = new DOFFloat32(dy);
            forceFields[2] = new DOFFloat32(dz);
            forceFields[3] = new DOFFloat64(magnitude);
            DOFValue forces = new DOFStructure(ImpactInterface.Force, forceFields);

            List<DOFValue> values = Arrays.asList(location, forces);

            synchronized (_monitor) {
                if (_providerObject != null)
                    _providerObject.signal(ImpactInterface.eventImpact, values);
                else
                    throw new Exception("_providerObject not created.");
            }
        }
        else {
            //_logger.info("Impact event (under _threshold): " + magnitude);
            throw new Exception("Impact event (under _threshold): " + magnitude + ", _threshold: " + _data.getThreshold());
        }
    }

    @Override
    public void setWeight(double weight)
    {
        //_logger.info("Weight set to: " + weight);
        _data.setWeight(weight);

        if (_onSuitcaseChangedListener != null)
            _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.WEIGHT, _data);

        synchronized (_monitor) {
            if (_providerObject != null)
                _providerObject.changed(WeightInterface.propertyWeight);
        }
    }

    @Override
    public double getWeight()
    {
        return _data.getWeight();
    }


    // ********** Provider **********
    @Override
    public void get(Provide operation, Get request, Property property)
    {
        //_logger.info("Get called for property " + property + " on interface " + property.getInterface());

        if (property.getInterfaceID().equals(WeightInterface.iid)) {
            if (property.getItemID() == 1) {
                final DOFValue value = new DOFFloat64(_data.getWeight());
                request.respond(value);
                return;
            }
        }
        else if (property.getInterfaceID().equals(GPSLocationInterface.iid)) {
            if (property.getItemID() == 1) {
                DOFValue[] fields = new DOFValue[3];
                fields[0] = new DOFFloat64(_data.getLatitude());
                fields[1] = new DOFFloat64(_data.getLongitude());
                fields[2] = new DOFFloat64(_data.getElevation());

                DOFStructure struct = new DOFStructure(GPSLocationInterface.Location, fields);
                request.respond(struct);
                return;
            }
        }
        else if (property.getInterfaceID().equals(LockableInterface.iid)) {
            if (property.getItemID() == 3) {
                final DOFValue value = new DOFBoolean(_data.isLocked());
                request.respond(value);
                return;
            }
        }
        else if (property.getInterfaceID().equals(ImpactInterface.iid)) {
            if (property.getItemID() == 2) {
                final DOFValue value = new DOFFloat64(_data.getThreshold());
                request.respond(value);
                return;
            }
        }

        //_logger.info("Attempted Get for unrecognized property: " + property + " on interface " + property.getInterface());

        request.respond(new DOFNotFoundException("Property not readable: " + property));
    }

    @Override
    public void set(Provide operation, Set request, Property property, DOFValue value)
    {
        if (property.getInterfaceID().equals(ImpactInterface.iid)) {
            if (property.getItemID() == ImpactInterface.PROPERTY_THRESHOLD) {
                setThreshold(((DOFFloat64) value).get());
                request.respond();
                return;
            }
        }
        else if(property.getInterfaceID().equals(WeightInterface.iid)){
            setWeight(((DOFFloat64)value).get());
            request.respond();
            return;
        }

        request.respond(new DOFNotFoundException("Property not settable: " + property));
    }

    @Override
    public void invoke(Provide operation, Invoke request, Method method, List<DOFValue> parameters)
    {
        if (method.getInterfaceID().equals(LockableInterface.iid)) {
            if (method.getItemID() == ImpactInterface.EVENT_IMPACT) {
                lock();
                request.respond();
                return;
            }
            else if (method.getItemID() == ImpactInterface.PROPERTY_THRESHOLD) {
                unlock();
                request.respond();
                return;
            }
        }

        request.respond(new DOFNotFoundException("Method not invokable: " + method));
    }

    @Override
    public void subscribe(Provide operation, Subscribe request, Property property, int minPeriod)
    {
        //_logger.info("Subscription started for property " + property + " on interface " + property.getInterface());
        request.respond();
    }

    @Override
    public void subscribeComplete(Provide operation, Subscribe request, Property property)
    {

    }

    @Override
    public void register(Provide operation, Register request, Event event)
    {
        //_logger.info("Registration started for event " + event + " on interface " + event.getInterface());
        //Log.d("SuitcaseActivityController", "Registration started for event " + event + " on interface " + event.getInterface());
        request.respond();
    }

    @Override
    public void registerComplete(Provide operation, Register request, Event event)
    {

    }

    @Override
    public void session(Provide operation, Session request, DOFObject object, DOFInterfaceID interfaceID, DOFObjectID sessionID, DOFInterfaceID sessionType)
    {
        request.respond(new DOFApplicationErrorException("'session' method not implemented on _providerObject."));
    }

    @Override
    public void sessionComplete(Provide operation, Session request, DOFObject object, DOFInterfaceID interfaceID, DOFObjectID sessionID, DOFInterfaceID sessionType)
    {

    }

    // *************** DOFOperation.OperationListener ****************
    @Override
    public void complete(DOFOperation operation, DOFException exception)
    {
        if (exception != null && _onSuitcaseChangedListener != null && operation == _weightProvide) {
            _data.setDofSystem(_dofSystem);
            _onSuitcaseChangedListener.onSuitcaseChanged(IOnSuitcaseChangedListener.Types.EXCEPTION, _data);
        }
    }
}
