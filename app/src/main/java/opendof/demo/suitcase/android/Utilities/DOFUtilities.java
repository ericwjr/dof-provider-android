package opendof.demo.suitcase.android.Utilities;

import org.opendof.core.oal.DOF;
import org.opendof.core.oal.DOFAddress;
import org.opendof.core.oal.DOFConnection;
import org.opendof.core.oal.DOFCredentials;
import org.opendof.core.oal.DOFDomain;
import org.opendof.core.oal.DOFObjectID;
import org.opendof.core.oal.DOFSystem;
import org.opendof.core.transport.inet.InetTransport;

/**
 * <h1>DOF Utilities</h1>
 * This implements various static methods for creating secured DOF node, system,
 * credentials, domain, connection, server.
 * <p></p>.
 * <b>Note: </b>params must not be null.
 *
 * @author zdusatko - upgraded
 * @since 01/13/2015
 */
public class DOFUtilities
{
    /**
     * Shared timeout value for various asynchronous operations
     */
    public static final int TIMEOUT = 15000;

    /**
     * Creates secured DOF domain.
     *
     * @return DOF This returns DOF networking node
     */
    public static DOF createDOF()
    {
        return new DOF();
    }

    /**
     * Creates secured DOF domain.
     *
     * @param credentials Authentication
     * @param dof         DOF networking node
     * @return DOFDomain This returns DOF domain
     * @throws Exception
     */
    public static DOFDomain createSecuredDOFDomain(DOFCredentials credentials, DOF dof) throws Exception
    {
        DOFDomain.Config domainConfig;
        DOFDomain domain;
        try {
            domainConfig = new DOFDomain.Config.Builder(credentials)
                    .setMaxSilence(TIMEOUT)
                    .build();

            domain = dof.createDomain(domainConfig);
        }
        catch (Exception e) {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Exception thrown when creating DOF domain: " + e.getMessage());
            throw e;
        }

        return domain;
    }

    /**
     * Creates secured DOF system.
     *
     * @param systemName Name of the system
     * @param credentials Authentication
     * @param dof DOF networking node
     * @return DOFSystem This returns DOF system
     * @throws Exception
     */
    public static DOFSystem createSecuredDOFSystem(String systemName, DOFCredentials credentials, DOF dof) throws Exception
    {
        DOFSystem.Config systemConfig;
        DOFSystem system;

        try {
            systemConfig = new DOFSystem.Config.Builder()
                    .setCredentials(credentials) //add the credentials to the system config
                    .setName(systemName)
                    .build();

            system = dof.createSystem(systemConfig);
        }
        catch (Exception e) {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Exception thrown when creating DOF system: " + e.getMessage());
            throw e;
        }

        return system;
    }

    /**
     * Creates secured DOF connection.
     *
     * @param addressString String representation of an Inet transport address.
     * @param port Port number
     * @param credentials Authentication
     * @param dof DOF networking node
     * @return DOFConnection This returns DOF connection
     * @throws Exception
     */
    public static DOFConnection createSecuredConnection(String addressString, int port, DOFCredentials credentials, DOF dof) throws Exception
    {
        DOFAddress addressDOF;
        DOFConnection.Config connectionConfig;
        DOFConnection connection;

        try {
            addressDOF = InetTransport.createAddress(addressString, port);
            connectionConfig = new DOFConnection.Config.Builder
                    (DOFConnection.Type.STREAM, addressDOF)
                    .setCredentials(credentials)
                    .setSecurityDesire(DOF.SecurityDesire.SECURE)
                    .build();

            connection = dof.createConnection(connectionConfig);
        }
        catch (Exception e) {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Exception thrown when creating DOF connection: " + e.getMessage());
            throw e;
        }

        return connection;
    }

    /**
     * @param domainOIDString
     * @param identityIDString
     * @param secret
     * @return DOFCredentials
     * @throws Exception
     */
    public static DOFCredentials createCredentials(String domainOIDString, String identityIDString, String secret) throws Exception
    {
        DOFObjectID.Domain domain;
        DOFObjectID.Authentication authentication;
        DOFCredentials credentials;

        try {
            domain = DOFObjectID.Domain.create(domainOIDString);
            authentication = DOFObjectID.Authentication.create(identityIDString);
            credentials = DOFCredentials.Password.create(domain, authentication, secret);
        }
        catch (Exception e) {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Exception thrown when creating DOF credentials: " + e.getMessage());
            throw e;
        }

        return credentials;
    }
}
