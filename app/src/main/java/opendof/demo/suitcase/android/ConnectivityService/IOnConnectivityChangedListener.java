package opendof.demo.suitcase.android.ConnectivityService;

import org.opendof.core.oal.DOFSystem;

/**
 * <h1>Listener for connectivity changes</h1>
 * <p></p>.
 *
 * @author zdusatko
 * @since 7/31/2015
 */
public interface IOnConnectivityChangedListener
{
    public enum Types
    {
        DOFSYSTEM_CREATE,
        CONNECT,
        DISCONNECT,
        FATAL_EXCEPTION,
        NONFATAL_EXCEPTION
    }

    /**
     * This will track most of the changes to DOF system and connection
     *
     * @param connectivityChangedListener
     * @param dofSystem
     * @param msg
     */
    void onConnectivityChanged(Types connectivityChangedListener, DOFSystem dofSystem, String msg);
}
