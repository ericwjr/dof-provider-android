package opendof.demo.suitcase.android.TempCredentials;

import org.opendof.core.oal.DOFInterfaceID;
import org.opendof.core.oal.DOFObjectID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class DOFObjectInitializationInformation {

    DOFObjectID.Domain domain;
    DOFObjectID.Authentication identity;
    String password;
    HashMap<DOFObjectID, ArrayList<DOFInterfaceID>> bindings;

    public DOFObjectID.Domain getDomain() {
        return domain;
    }

    public void setDomain(DOFObjectID.Domain domain) {
        this.domain = domain;
    }

    public DOFObjectID.Authentication getIdentity() {
        return identity;
    }

    public void setIdentity(DOFObjectID.Authentication identity) {
        this.identity = identity;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DOFObjectID getDOFObjectID()
    {
        DOFObjectID firstOID = null;
        Iterator<DOFObjectID> iterator = bindings.keySet().iterator();
        if(iterator.hasNext())
            firstOID = iterator.next();

        return firstOID;
    }

    public HashMap<DOFObjectID, ArrayList<DOFInterfaceID>> getBindings() {
        return bindings;
    }

    public ArrayList<DOFInterfaceID> getBinding(DOFObjectID oid) {
        if(bindings != null){
            if(bindings.containsKey(oid)){
                return bindings.get(oid);
            }
        }
        return null;
    }

    public DOFObjectInitializationInformation() {

    }

    public void addBinding(DOFObjectID oid, DOFInterfaceID iid){
        if(bindings == null)
            bindings = new HashMap<>(1);

        if(bindings.containsKey(oid)){
            ArrayList<DOFInterfaceID> interfaces = bindings.get(oid);
            if(interfaces.contains(iid))
                return;
            else
                interfaces.add(iid);
        } else {
            ArrayList<DOFInterfaceID> interfaces = new ArrayList<DOFInterfaceID>(1);
            interfaces.add(iid);
            bindings.put(oid, interfaces);
        }
    }

    public void removeBinding(DOFObjectID oid, DOFInterfaceID iid){
        if(bindings == null)
            return;

        if(bindings.containsKey(oid)){
            ArrayList<DOFInterfaceID> interfaces = bindings.get(oid);
            if(interfaces.contains(iid)){
                interfaces.remove(iid);
                if(interfaces.isEmpty()){
                    bindings.remove(oid);
                }
            }
        }
    }

    @Override
    public String toString() {
        String output = "DOFObjectInitializationInformation [domain=" + domain
                + ", identity=" + identity + ", password=" + password
                + ", bindings=";
        for(Entry<DOFObjectID, ArrayList<DOFInterfaceID>> entry: bindings.entrySet()){
            output += " " + entry.getKey() + ": [";
            for(DOFInterfaceID iid : entry.getValue()){
                output += iid + ", ";
            }
            output.substring(0, output.length() - 2);
            output += "], ";
        }
        output.substring(0, output.length() - 2);

        output += "]";

        return output;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((bindings == null) ? 0 : bindings.hashCode());
        result = prime * result + ((domain == null) ? 0 : domain.hashCode());
        result = prime * result
                + ((identity == null) ? 0 : identity.hashCode());
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DOFObjectInitializationInformation other = (DOFObjectInitializationInformation) obj;
        if (bindings == null) {
            if (other.bindings != null)
                return false;
        } else if (!bindings.equals(other.bindings))
            return false;
        if (domain == null) {
            if (other.domain != null)
                return false;
        } else if (!domain.equals(other.domain))
            return false;
        if (identity == null) {
            if (other.identity != null)
                return false;
        } else if (!identity.equals(other.identity))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        return true;
    }


}