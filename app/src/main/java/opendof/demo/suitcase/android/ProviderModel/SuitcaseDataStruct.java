package opendof.demo.suitcase.android.ProviderModel;

import org.opendof.core.oal.DOFSystem;

/**
 * <h1>Suitcase data structure</h1>
 * This represents all data that suitcase provider need to share
 * <p></p>.
 *
 * @author zdusatko
 * @since 2/20/2015
 */
public class SuitcaseDataStruct
{
    public static final String LOG_TAG = "Suitcase";

    /**
     * This is a group of concise suitcase states
     */
    public enum SuitcaseState
    {
        RED, // not connected, temporary credentials fail
        YELLOW, // is connected, can't provide, doesn't have a suitcase ID
        GREEN // is connected, can provide, has a suitcase ID
    }

    private DOFSystem _dofSystem = null;
    private Exception _exception = null;
    private double _weight;
    private boolean _isLocked;
    private double _longitude;
    private double _latitude;
    private double _elevation;
    private double _threshold;
    private float _impactX;
    private float _impactY;
    private float _impactZ;
    private double _impactMagnitude;
    private SuitcaseState _state = null;

    public DOFSystem getDofSystem() { return _dofSystem; }

    public void setDofSystem(DOFSystem dofSystem) { _dofSystem = dofSystem; }

    public Exception getException() { return _exception;  }

    public void setException(Exception exception) { _exception = exception; }

    public double getWeight()
    {
        return _weight;
    }

    public boolean isLocked()
    {
        return _isLocked;
    }

    public double getLongitude()
    {
        return _longitude;
    }

    public double getLatitude()
    {
        return _latitude;
    }

    public double getElevation()
    {
        return _elevation;
    }

    public double getThreshold()
    {
        return _threshold;
    }

    public double getImpactX()
    {
        return _impactX;
    }

    public double getImpactY()
    {
        return _impactY;
    }

    public double getImpactZ()
    {
        return _impactZ;
    }

    public double getImpactMagnitude()
    {
        return _impactMagnitude;
    }

    public SuitcaseState getState()
    {
        return _state;
    }

    public void setWeight(double weight)
    {
        _weight = weight;
    }

    public void setLocked(boolean isLocked)
    {
        _isLocked = isLocked;
    }

    public void setLongitude(double longitude)
    {
        _longitude = longitude;
    }

    public void setLatitude(double latitude)
    {
        _latitude = latitude;
    }

    public void setElevation(double elevation)
    {
        _elevation = elevation;
    }

    public void setThreshold(double threshold)
    {
        _threshold = threshold;
    }

    public void setImpactX(float impactX)
    {
        _impactX = impactX;
    }

    public void setImpactY(float impactY)
    {
        _impactY = impactY;
    }

    public void setImpactZ(float impactZ)
    {
        _impactZ = impactZ;
    }

    public void setImpactMagnitude(double impactMagnitude)
    {
        _impactMagnitude = impactMagnitude;
    }

    public void setState(SuitcaseState state)
    {
        _state = state;
    }

    public SuitcaseDataStruct()
    {

    }
}
