/*
 * This work is protected by Copyright, see COPYING.html for more information.
 */

package opendof.demo.suitcase.android.Controller;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.panasonic.monov.cstdemo.R;

import org.opendof.core.oal.DOFSystem;

import opendof.demo.suitcase.android.ConnectivityService.IOnConnectivityChangedListener;
import opendof.demo.suitcase.android.ConnectivityService.SuitcaseConnectivityService;
import opendof.demo.suitcase.android.ProviderModel.IOnSuitcaseChangedListener;
import opendof.demo.suitcase.android.ProviderModel.SuitcaseDataStruct;
import opendof.demo.suitcase.android.ProviderModel.SuitcaseProviderModel;
import opendof.demo.suitcase.android.TempCredentials.IOnCredentialsChangedListener;
import opendof.demo.suitcase.android.Utilities.MyTools;
import opendof.demo.suitcase.android.Views.IOnDataInputListener;
import opendof.demo.suitcase.android.Views.IOnOKClickedListener;
import opendof.demo.suitcase.android.Views.SuitcaseView;
import opendof.demo.suitcase.android.TempCredentials.CredentialsRequestor;
import opendof.demo.suitcase.android.TempCredentials.DOFObjectInitializationInformation;
import opendof.demo.suitcase.android.Views.SettingsView;

import java.util.concurrent.Executors;

/**
 * <h1>Suitcase Controller</h1>
 * This is the main suitcase controller that is built on Android Activity.
 * It controls wifi connections, DOF connectivity as a background service, reads phone sensors and
 * calls another Activity that handles scanning credential info if required by a user.
 * <p></p>.
 *
 * @author zdusatko
 * @since 2/20/2015
 */
public class SuitcaseActivityController extends ActionBarActivity
        implements
        SensorEventListener,
        LocationListener,
        IOnConnectivityChangedListener,
        IOnSuitcaseChangedListener,
        IOnDataInputListener,
        IOnCredentialsChangedListener {
    private String _host;
    private int _port;
    private String _oid;
    private String _domain;
    private String _identity;
    private String _password;

    private String _IMEI_id = null;
    private String _MAC_adress = null;

    private DOFSystem _dofSystem = null;

    private SharedPreferences _sharedPreferences = null;
    private final static int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 2;
    private static final String OUTPUT_FORMAT = "%.3f";
    private static final String HELP_URL = "http://opendof.org/suitcase-help/";
    // this is minimum distance that GPS will register as a change of location
    private static final float MIN_DISTANCE = 2.0f; // in meters
    private static int IMPACT_DELAY = 1000;
    private static int IMPACT_MEASURE_RATE = 3000000; // in microseconds, every 3 seconds, this is only a hint to the system
    private static int LOCATION_MIN_TIME = 5000;

    private SensorManager _sensorManager;
    private Sensor _accelerometer;

    private LocationManager _locationManager;
    private Location _lastLocation = null;

    private long _lastImpactTimestamp;

    private WifiManager _wifiManager = null;
    private WifiManager.WifiLock _wifiLock = null;

    private SuitcaseView _suitcaseView = null;
    private SettingsView _settingsView;

    private Toast _toast = null; // will be used to cancel the current toast and start another one

    // thread management
    private AsyncReload _asyncReload = null; // connectivity service thread
    private AsyncGetTempCredentialsTask _asyncGetTempCredentialsTask = null; // used to get temp credentials requestor

    /**
     * Called when the activity is starting. This is where most initialization should go.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get unique phone identifier
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

       if(!MyTools.checkPhonePermsNoRequest(getApplicationContext()))
       {
           ActivityCompat.requestPermissions(this,
                   new String[]{Manifest.permission.READ_PHONE_STATE},
                   MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
       }else{
           _IMEI_id = tm.getDeviceId();
       }

        // Make sure it keeps internet connection on
        _wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        // keep wifi radio awake
        _wifiLock = _wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "LockTag");

        // extract MAC address in case IMEI is not available
        WifiInfo wInfo = _wifiManager.getConnectionInfo();
        _MAC_adress = wInfo.getMacAddress();

        _suitcaseView = new SuitcaseView(this);
        setContentView(_suitcaseView);

        // force action bar icon
        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.drawable.icon);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        _sharedPreferences = getSharedPreferences("suitcase_prefs", Context.MODE_PRIVATE);

        // initialize credentials from stored shared preferences if any
        _host = _sharedPreferences.getString("host", "");
        _port = _sharedPreferences.getInt("port", 0);
        _oid = _sharedPreferences.getString("oid", "");
        _domain = _sharedPreferences.getString("domain", "");
        _identity = _sharedPreferences.getString("identity", "");
        _password = _sharedPreferences.getString("password", "");

        // initialize android sensors
        _sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        _accelerometer = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        _locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // receive message from bar code scanner CaptureActivity
        String messageFromScanner = receiveMessageFromCaptureActivity();

        // start listening for suitcase data change
        SuitcaseProviderModel.getInstance().setOnSuitcaseChangedListener(this);
        // start listening to suitcase_menu view input
        _suitcaseView.setOnDataInputListener(this);

        // check if received credentials ok if not open settings dialog
        if (messageFromScanner != null)
            setCredentialsFromScanner(messageFromScanner);

        // start connectivity service as a background service
        Intent i = new Intent(this, SuitcaseConnectivityService.class);
        startService(i);

        // show settings dialog only if it is not providing already
        if (SuitcaseProviderModel.getInstance().getState() != SuitcaseDataStruct.SuitcaseState.GREEN || messageFromScanner != null)
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showSettingsDialog();
                }
            });

    }

    private void callCaptureActivity() {
//        Intent scannerIntent = new Intent(SuitcaseActivityController.this, CaptureActivity.class);
//        startActivity(scannerIntent);
    }

    private String receiveMessageFromCaptureActivity() {
        Intent scannerIntent = getIntent();
//        if (scannerIntent.hasExtra(CaptureActivity.BARCODE_MESSAGE)) {
        //Log.d(SuitcaseDataStruct.LOG_TAG, "credentials from scanner received");
//            return scannerIntent.getStringExtra(CaptureActivity.BARCODE_MESSAGE);
//        }

        return null;
    }

    private void updateView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SuitcaseProviderModel suitcase = SuitcaseProviderModel.getInstance();
                _suitcaseView.updateConnectionIcon(suitcase.getState());

                _suitcaseView.updateOID(_oid);

                _suitcaseView.updateLocked(suitcase.isLocked());
                _suitcaseView.updateWeight(suitcase.getWeight());

                //if (suitcase.getState() != SuitcaseDataStruct.SuitcaseState.RED)
                _suitcaseView.updateLocation(suitcase.getLatitude(), suitcase.getLongitude(), suitcase.getElevation());
            }
        });

    }

    /**
     * Graps scanned message and updates credentials
     * @param messageFromScanner
     */
    private void setCredentialsFromScanner(String messageFromScanner) {
        String[] credentials = messageFromScanner.split("\\|");

        // assuming this format:
        // dsp.emit-networking.org|3567|[3:suitcase_menu-2@demo.opendof.org]|[6:demo.OpenDOF.org]|[3:suitcase_menu-2@demo.opendof.org]|e210272c
        _host = credentials[0];
        _port = Integer.parseInt(credentials[1]);
        _oid = credentials[2];
        _domain = credentials[3];
        _identity = credentials[4];
        _password = credentials[5];
    }

    private void saveSharedPreferences() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // update shared preferences
                SharedPreferences.Editor prefEditor = _sharedPreferences.edit();
                // host
                prefEditor.putString("host", _host);
                // port
                prefEditor.putInt("port", _port);
                // oid
                prefEditor.putString("oid", _oid);
                // domainID
                prefEditor.putString("domain", _domain);
                // identity
                prefEditor.putString("identity", _identity);
                // password
                prefEditor.putString("password", _password);
                prefEditor.commit();
            }
        });

    }

    private void reload() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showToast("Connecting...", true);
                SuitcaseProviderModel.getInstance().setState(SuitcaseDataStruct.SuitcaseState.YELLOW);
                updateView();
                // android OS requests any network connections on a separate thread
                if (_asyncReload != null)
                    _asyncReload.cancel(true); // cancel first

                _asyncReload = new AsyncReload();
                //_asyncReload.execute((Void) null);
                // Forcing it to a single thread
                _asyncReload.executeOnExecutor(Executors.newSingleThreadExecutor(), (Void) null);
                //Log.d(SuitcaseDataStruct.LOG_TAG, "reload() called");
            }
        });
    }


    private class AsyncReload extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "doInBackground called");
            SuitcaseConnectivityService service = SuitcaseConnectivityService.getInstance();
            if (service != null) {
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Suitcase connectivity service found");
                service.setOnConnectivityChangedListener(SuitcaseActivityController.this);
                service.connectDOF(_host, _port, _oid, _domain, _identity, _password);
            } else {
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Service not ready, ");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Log.d(SuitcaseDataStruct.LOG_TAG, "Exception thread e: " + e.getMessage());
                }

                reload();
            }

            return null;
        }
    }

    // ************** Activity **************
    @Override
    protected void onResume() {
        super.onResume();
        // Accelerometer
        _sensorManager.registerListener(this, _accelerometer, IMPACT_MEASURE_RATE);
        //Log.d(SuitcaseDataStruct.LOG_TAG, "_accelerometer registered.");

        // GPS location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        _locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_MIN_TIME, MIN_DISTANCE, this);
        Location loc = _locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (loc != null)
            this.onLocationChanged(loc);
    }

    @Override
    protected void onPause() {
        super.onPause();
        _sensorManager.unregisterListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        _locationManager.removeUpdates(this);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        //Log.d(SuitcaseDataStruct.LOG_TAG, "destroyed");
        if (isFinishing()) {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "finished");
            Intent i = new Intent(this, SuitcaseConnectivityService.class);
            stopService(i);

            // cancel all running threads if any
            if (_asyncReload != null)
                _asyncReload.cancel(true); // cancel first

            if (_asyncGetTempCredentialsTask != null)
                _asyncGetTempCredentialsTask.cancel(true);

            // destroyConnection suitcase_menu, since "this" object is multiple listeners they will get destroyConnection automatically
            SuitcaseProviderModel.getInstance().destroy();

            // remove lock on wifi but some other app may have its lock so do try-catch
            try {
                if (_wifiLock != null)
                    _wifiLock.release();
            }
            catch (Exception e) {
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Wifi lock was probably already released Exception: " + e.getMessage());
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.suitcase_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                showSettingsDialog();
                return true;
            case R.id.about:
                showAboutView();
                return true;
            case R.id.help:
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(HELP_URL));
                startActivity(myIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAboutView()
    {
        // just inflate about view
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View aboutView = inflater.inflate(R.layout.about_view, null);

        final AlertDialog.Builder copyrightDialog = new AlertDialog.Builder(this);
        copyrightDialog.setView(aboutView);

        copyrightDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.cancel();
            }
        });
        copyrightDialog.setTitle("About");
        AlertDialog alert = copyrightDialog.create();
        alert.show();
    }

    // ************** SensorEventListener **************
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        long current = System.currentTimeMillis();

        if (current - _lastImpactTimestamp >= IMPACT_DELAY) {
            _lastImpactTimestamp = current;
            SuitcaseProviderModel suitcase = SuitcaseProviderModel.getInstance();
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Impact registered at: " + current/1000 + " s");
            if (suitcase.getState() == SuitcaseDataStruct.SuitcaseState.GREEN) {
                float ax = event.values[0];
                float ay = event.values[1];
                float az = event.values[2];

                float magnitude = (float) Math.sqrt(ax * ax + ay * ay + az * az);

                float dx = ax / magnitude;
                float dy = ay / magnitude;
                float dz = az / magnitude;

                //Put the magnitude in g's.
                magnitude = magnitude / SensorManager.GRAVITY_EARTH;

                if (magnitude >= suitcase.getThreshold()) {
                    //Log.d(SuitcaseDataStruct.LOG_TAG, "sending impact.");
                    try {
                        suitcase.impactEvent(dx, dy, dz, magnitude);
                        //showToast(String.format("Sending Impact Event: %fg", magnitude));
                    }
                    catch (Exception e) {
                        Log.d(SuitcaseDataStruct.LOG_TAG, "Error when sending impact event, error: " + e.getMessage());
                        showToast("Error when sending impact event, error: " + e.getMessage(), false);
                    }
                }
            }
        }
    }

    // ************** LocationListener **************
    @Override
    public void onLocationChanged(Location location)
    {
        // calculate distance between new and old locations
        float distance = 0f;
        if (_lastLocation != null)
            distance = _lastLocation.distanceTo(location);

        // update only if distance changed or the first time getting location
        if (distance >= MIN_DISTANCE || _lastLocation == null) {
            SuitcaseProviderModel.getInstance().setLocation(location.getLongitude(), location.getLatitude(), location.getAltitude());
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Location changed: " + location.toString());
            _lastLocation = location;
            updateView();
        }
    }

    @Override
    public void onProviderDisabled(String provider)
    {

    }

    @Override
    public void onProviderEnabled(String provider)
    {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    // ***************** IOnConnectivityChangedListener *****************

    /**
     * Listener for most of DOF connectivity changes
     *
     * @param connectivityChangedListener
     * @param dofSystem
     * @param message
     */
    @Override
    public void onConnectivityChanged(IOnConnectivityChangedListener.Types connectivityChangedListener, DOFSystem dofSystem, String message)
    {
        switch(connectivityChangedListener)
        {
            case DOFSYSTEM_CREATE:
                if (dofSystem != null)
                {
                    // all green
                    _dofSystem = dofSystem;
                    SuitcaseProviderModel suitcase = SuitcaseProviderModel.getInstance();
                    // start listening to suitcase changes
                    suitcase.setOnSuitcaseChangedListener(this);
                    suitcase.startProvide(_oid, dofSystem);
                }

                // update view
                updateView();
                break;

            case FATAL_EXCEPTION:
            case NONFATAL_EXCEPTION:
                SuitcaseProviderModel.getInstance().setState(SuitcaseDataStruct.SuitcaseState.RED);
                showToast("Connection failed: " + message, false);
                updateView();
                break;

            case CONNECT:
                showToast("Suitcase connected", false);
                updateView();
                break;

            case DISCONNECT:
                SuitcaseProviderModel.getInstance().setState(SuitcaseDataStruct.SuitcaseState.RED);
                showToast("Suitcase disconnected", false);
                updateView();
                break;
        }
    }

    private class AsyncGetTempCredentialsTask extends AsyncTask<DOFSystem, Void, Void>
    {
        @Override
        protected Void doInBackground(DOFSystem... dofSystems)
        {
            DOFSystem dofSystem = dofSystems[0]; // passing only 1 argument

            // create temporary credentials
            SuitcaseProviderModel.getInstance().setState(SuitcaseDataStruct.SuitcaseState.YELLOW);

            _oid = createUniqueOID();
            if (_oid == null) {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showSettingsDialog();
                        showToast("Couldn't retrieve MAC address", false);
                    }
                });

                _asyncGetTempCredentialsTask.cancel(true);
                return null;
            }

            // 2. find temp credentials provider and get new temp credentials
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Getting temp credentials");
            showToast("Getting temp credentials", false);
            CredentialsRequestor credentialsRequestor = CredentialsRequestor.getInstance();
            credentialsRequestor.setOnCredentialsChangedListener(SuitcaseActivityController.this);
            credentialsRequestor.init(_oid, dofSystem);

            return null;
        }
    }

    private String createUniqueOID()
    {
        String uniqueOID = null;

        // 1. create new suitcase_menu ID, using 14-digit IMEI number or 12-digit MAC address
        if (_IMEI_id != null) {
            uniqueOID = "[14:{" + _IMEI_id.substring(0, 14) + "}]";
        }
        else if (_MAC_adress != null) {
            uniqueOID = _MAC_adress.replace(":", ""); // remove colons
            uniqueOID = "[2:{" + uniqueOID + "}]";
        }

        return uniqueOID;
    }

    // ************** IOnCredentialsChangedListener ************
    @Override
    public void onCredentialsChanged(IOnCredentialsChangedListener.Types types, DOFObjectInitializationInformation objectInfo, Exception e)
    {
        switch(types)
        {
            case ACQUIRE:
                if (objectInfo != null) {
                    // new temporary credentials
                    _oid = objectInfo.getDOFObjectID().toStandardString();
                    _domain = objectInfo.getDomain().toStandardString();
                    _identity = objectInfo.getIdentity().toStandardString();
                    _password = objectInfo.getPassword();

                    saveSharedPreferences();

                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            _settingsView.updateViewCredentials(_host, _port, _oid, _domain, _identity, _password);
                        }
                    });

                    reload();
                }
                else {
                    SuitcaseProviderModel.getInstance().setState(SuitcaseDataStruct.SuitcaseState.YELLOW);
                    //Log.d(SuitcaseDataStruct.LOG_TAG, "Temporary credentials failed");
                    //showToast("Temporary credentials failed", false);
                    updateView();
                }
                break;

            case EXCEPTION:
                SuitcaseProviderModel.getInstance().setState(SuitcaseDataStruct.SuitcaseState.YELLOW);
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Temporary credentials failed: " + e.getMessage());
                //showToast("Temporary credentials failed: " + e.getMessage(), false);
                updateView();
                break;
        }

    }

    // ***************** IOnSuitcaseChangedListener *****************
    @Override
    public void onSuitcaseChanged(IOnSuitcaseChangedListener.Types types, final opendof.demo.suitcase.android.ProviderModel.SuitcaseDataStruct suitcaseDataStruct)
    {
        switch (types) {
            case IMPACT:
                showToast("Impact registered: " + String.format(OUTPUT_FORMAT, suitcaseDataStruct.getImpactMagnitude()) + " G", false);
                break;

            case GPS:
                showToast("GPS changed", false);
                break;

            case WEIGHT:
                showToast("Weight changed: " + String.format(OUTPUT_FORMAT, suitcaseDataStruct.getWeight()) + " Kg", false);
                break;

            case LOCK:
                showToast("Lock changed", false);
                break;

            case THRESHOLD:
                showToast("Impact threshold changed: " + String.format(OUTPUT_FORMAT, suitcaseDataStruct.getThreshold()) + " G", false);
                break;

            case EXCEPTION:
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //Log.d(SuitcaseDataStruct.LOG_TAG, "Credentials failed");
                        //showToast("Credentials failed", false);
                        // assuming it is still connected but provide operation failed
                        SuitcaseProviderModel.getInstance().setState(SuitcaseDataStruct.SuitcaseState.YELLOW);
                        updateView();

                        // android OS requests any network connections on a separate thread
                        if (_asyncGetTempCredentialsTask != null)
                            _asyncGetTempCredentialsTask.cancel(true); // cancel first

                        _asyncGetTempCredentialsTask = new AsyncGetTempCredentialsTask();
                        // Forcing it to a single thread
                        _asyncGetTempCredentialsTask.executeOnExecutor(Executors.newSingleThreadExecutor(), suitcaseDataStruct.getDofSystem());
                    }
                });
                break;

            case PROVIDE:
                SuitcaseProviderModel.getInstance().setState(SuitcaseDataStruct.SuitcaseState.GREEN);
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Suitcase began providing data");
                showToast("Suitcase began providing data", false);
                updateView();
                break;
        }

        if(types.equals(IOnSuitcaseChangedListener.Types.PROVIDE))
        {

        }

        updateView();
    }

    // **************** IOnDataInputListener **************
    @Override
    public void OnDataInput(int buttonId)
    {
        SuitcaseProviderModel suitcase = SuitcaseProviderModel.getInstance();
        switch (buttonId) {
            case R.id.increase_weight:
                suitcase.setWeight(suitcase.getWeight() + 1);
                break;

            case R.id.decrease_weight:
                if (suitcase.getWeight() > 0)
                    suitcase.setWeight(suitcase.getWeight() - 1);
                break;

            case R.id.lock:
                suitcase.lock();
                break;

            case R.id.unlock:
                suitcase.unlock();
                break;
        }

        updateView();
    }

    // ************* HELPERS ***********

    /**
     * Shows short pop up message on screen
     * @param message
     * @param isLong
     */
    public void showToast(final String message, final boolean isLong)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                int duration = isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
                if (_toast != null) {
                    _toast.cancel();
                }

                _toast = Toast.makeText(getApplicationContext(), message, duration);
                _toast.show();
            }
        });
    }

    private void showSettingsDialog()
    {
        saveSharedPreferences();
        _settingsView = new SettingsView(this);
        _settingsView.updateViewCredentials(_host, _port, _oid, _domain, _identity, _password);

        _settingsView.scannerButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callCaptureActivity();
            }
        });

        _settingsView.setOnOKClickedListener(new IOnOKClickedListener()
        {
            @Override
            public void onOKClicked(String host, int port, String oid, String domain, String identity, String password)
            {
                // first make sure that some settings actually changed
                boolean settingsIsEmpty =
                        (host.equals("")) && (port == 0) && (oid.equals("")) && (domain.equals("")) && (identity.equals("")) && (password.equals(""));

                boolean credentialsChanged = ((!_host.equals(host)) ||
                        (_port != port) ||
                        (!_oid.equals(oid)) ||
                        (!_domain.equals(domain)) ||
                        (!_identity.equals(identity)) ||
                        (!_password.equals(password)));

                boolean suitcaseIsProviding =
                        SuitcaseProviderModel.getInstance().getState() == SuitcaseDataStruct.SuitcaseState.GREEN;

                if (settingsIsEmpty) {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            showSettingsDialog();
                        }
                    });

                }
                else if (credentialsChanged || !suitcaseIsProviding) {
                    _host = host;
                    _port = port;
                    _oid = oid;
                    _domain = domain;
                    _identity = identity;
                    _password = password;

                    saveSharedPreferences();

                    _settingsView.updateViewCredentials(_host, _port, _oid, _domain, _identity, _password);
                    reload();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    _IMEI_id = tm.getDeviceId();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    MyTools.displayToastMessage(getApplicationContext(), "App cannot work without this permission");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
