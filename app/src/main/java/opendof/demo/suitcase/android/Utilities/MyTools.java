package opendof.demo.suitcase.android.Utilities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class MyTools {


    private static final String TAG = "MyTools";
    static Bitmap ReuseBitmap;
    /*
     * Displays a non-intrusive pop up message aka toast
     */
    public static void displayToastMessage(Context currCtx, String mytext){

        Toast.makeText(currCtx, mytext,
                Toast.LENGTH_LONG).show();
    }

    public static void showSnack(View view, String msg)
    {
        Snackbar snackbar = Snackbar
                .make(view, msg, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    public static void showSnackWithAction(View view, String snackTitle, View.OnClickListener callback, String actionText)
    {
        Snackbar snackbar = Snackbar
                .make(view, snackTitle, Snackbar.LENGTH_LONG)
                .setAction(actionText, callback);

        snackbar.show();
    }

    /*
     * Displays a non-intrusive pop up message aka toast
     */
    public static void displayShortToastMessage(Context currCtx, String mytext){

        Toast.makeText(currCtx, mytext,
                Toast.LENGTH_SHORT).show();
    }

    public static Bitmap ShrinkBitmap(String file, int width, int height)
    {
        // TODO Auto-generated method stub
        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        bmpFactoryOptions.inMutable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    public static Bitmap ShrinkBitmapReuse(String file, int width, int height)
    {
        // TODO Auto-generated method stub
        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        if(ReuseBitmap!=null)
            bmpFactoryOptions.inBitmap = ReuseBitmap;

        BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                // bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                // bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        ReuseBitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return ReuseBitmap;
    }


    public static boolean isNullOrEmpty( String value )
    {
        if ( value == null || value.trim().equalsIgnoreCase( "" ) )
        {
            return true;
        }
        return false;
    }

    public static boolean doesFileExist( String filePath )
    {
        if ( !isNullOrEmpty( filePath )  )
        {
            return new File( filePath ).exists();
        }
        return false;
    }

    public static String getStringByColumnIndex(Cursor cursor, int columnIndex )
    {
        if ( cursor.isNull( columnIndex ) )
        {
            return "";
        }
        return cursor.getString(columnIndex);
    }


    public static Long getLongByColumnIndex(Cursor cursor, int columnIndex )
    {
        if ( cursor.isNull( columnIndex ) )
        {
            return (long) -1;
        }
        return cursor.getLong(columnIndex);
    }

    public static Double getDoubleByColumnIndex(Cursor cursor, int columnIndex )
    {
        if ( cursor.isNull( columnIndex ) )
        {
            return (double) -1;
        }
        return cursor.getDouble(columnIndex);
    }

    public static Float getFloatByColumnIndex(Cursor cursor, int columnIndex )
    {
        if ( cursor.isNull( columnIndex ) )
        {
            return (float) -1;
        }
        return cursor.getFloat(columnIndex);
    }


    private static InputStream getAudioStream(String url )
    {
        try
        {
            return new URL( url ).openConnection().getInputStream();

        }
        catch ( MalformedURLException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
        // TODO Auto-generated method stub
        return null;
    }

    // DATABASE HELPER FUNCTIONS
    public static String getStringByColumnName(Cursor cursor, String columnName )
    {
        return getStringByColumnIndex( cursor, cursor.getColumnIndex( columnName ));
    }

    public static Long getLongByColumnName(Cursor cursor, String columnName ) throws RuntimeException
    {

        if(cursor.getColumnIndex(columnName) == -1)
            throw new RuntimeException();

        return getLongByColumnIndex(cursor, cursor.getColumnIndex(columnName));
    }

    public static Double getDoubleByColumnName(Cursor cursor, String columnName )
    {
        return getDoubleByColumnIndex(cursor, cursor.getColumnIndex(columnName));
    }

    public static Float getFloatByColumnName(Cursor cursor, String columnName )
    {
        return getFloatByColumnIndex(cursor, cursor.getColumnIndex(columnName));
    }


    public static boolean isNotNullAndShowing(Dialog order_dialog) {
        if(order_dialog!=null)
        {
            if(order_dialog.isShowing())
            {
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }


    public static boolean checkPhonePermsNoRequest(Context c) {
        int permissionCheck = ContextCompat.checkSelfPermission(c,
                Manifest.permission.READ_PHONE_STATE);
        if(permissionCheck!= PackageManager.PERMISSION_GRANTED)
        {
            return false;
        }
        else
            return true;

    }

    public static boolean checkSMSPermsNotRequest(Context c) {
        int permissionCheck = ContextCompat.checkSelfPermission(c,
                Manifest.permission.RECEIVE_SMS);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED)
        {
            return false;
        }
        else
            return true;
    }

    public static boolean checkStoragePermsNotRequest(Context c) {
        int permissionCheck = ContextCompat.checkSelfPermission(c,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED)
        {
            return false;
        }
        else
            return true;
    }

    public static boolean checkAccountsPermsNotRequest(Context c) {
        int permissionCheck = ContextCompat.checkSelfPermission(c,
                Manifest.permission.GET_ACCOUNTS);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED)
        {
            return false;
        }
        else
            return true;
    }

    public static boolean checkAllPerms(Context c)
    {
        return (checkPhonePermsNoRequest(c)&&checkAccountsPermsNotRequest(c)&& checkSMSPermsNotRequest(c) && checkStoragePermsNotRequest(c));
    }
}
