/*
 * This work is protected by Copyright, see COPYING.html for more information.
 */

package opendof.demo.suitcase.android.ConnectivityService;

import java.util.Date;

import org.opendof.core.oal.DOF;
import org.opendof.core.oal.DOF.Log.Level;
import org.opendof.core.oal.DOFConnection;
import org.opendof.core.oal.DOFConnection.State;
import org.opendof.core.oal.DOFCredentials;
import org.opendof.core.oal.DOFException;
import org.opendof.core.oal.DOFSystem;
import opendof.demo.suitcase.android.Utilities.DOFUtilities;
import org.opendof.core.ReconnectingStateListener;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * REVIEW: Javadocs
 */
public class SuitcaseConnectivityService
        extends Service
        implements DOF.Log.Listener, DOFSystem.StateListener, DOFConnection.StateListener
{
    private DOFCredentials _dofCredentials;
    private DOF _dof;
    private DOFConnection _dofConnection;
    private DOFSystem _dofSystem;
    private static ReconnectingStateListener _reconnectingStateListener = null;

    //REVIEW: add method for connection disconnect/reconnect

    private IOnConnectivityChangedListener _onConnectivityChangedListener = null;
    public void setOnConnectivityChangedListener(IOnConnectivityChangedListener onConnectivityChangedListener)
    {
        _onConnectivityChangedListener = onConnectivityChangedListener;
    }

    // Singleton
    public static SuitcaseConnectivityService getInstance()
    {
        return _instance;
    }
    private static SuitcaseConnectivityService _instance = null;

    /**
     * Sets credentials from user's input and tries to build DOF system and connection
     *
     * @param host
     * @param port
     * @param objectIDString
     * @param domainIDString
     * @param identityIDString
     * @param password
     */
    public void connectDOF(
            String host, int port, String objectIDString, String domainIDString, String identityIDString, String password)
    {
    	// REVIEW: Throw exception for parameter validation errors instead of onConnectionException?
    	// REVIEW: Validate input parameters before calling here?

        // make sure the existing connection is destroyed
        destroyConnection();

        // first check for basic connection formatting
        if( port > 65535 || port < 0 ) {
            if (_onConnectivityChangedListener != null)
                _onConnectivityChangedListener.onConnectivityChanged( opendof.demo.suitcase.android.ConnectivityService.IOnConnectivityChangedListener.Types.NONFATAL_EXCEPTION, null, "Port is out of range. Suggested is 3567"); //_onConnectionExceptionListener.onConnectionException("Port is out of range. Suggested is 3567");
            // this is serious so just jump out
            return;
        }

        try {
            // create secure connection
            _dofCredentials = DOFUtilities.createCredentials(domainIDString, identityIDString, password);
            _dofConnection = DOFUtilities.createSecuredConnection(host, port, _dofCredentials, _dof);
            _dofConnection.addStateListener(this);
        }
        catch (Exception e) {
            if (_onConnectivityChangedListener != null)
                _onConnectivityChangedListener.onConnectivityChanged(IOnConnectivityChangedListener.Types.FATAL_EXCEPTION, null, e.getMessage());
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Exception thrown when creating secured connection: ");
        }

        try {
            _dofConnection.connect(DOFUtilities.TIMEOUT);
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Secure connection connected");
        }
        catch (Exception e) {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Exception thrown when creating secured connection: " + e.getMessage());
            if (_reconnectingStateListener == null)
                _reconnectingStateListener = new ReconnectingStateListener();
            if(_dofConnection != null)
                _dofConnection.addStateListener(_reconnectingStateListener);

            if (_onConnectivityChangedListener != null)
                _onConnectivityChangedListener.onConnectivityChanged(IOnConnectivityChangedListener.Types.FATAL_EXCEPTION, null, e.getMessage());
        }
    }

    // *************** DOFSystem.StateListener ****************
    @Override
    public void stateChanged(DOFSystem dofSystem, DOFSystem.State state)
    {

        if(state.isAuthorized()) {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "Secure system authorized");
            if (_onConnectivityChangedListener != null)
                _onConnectivityChangedListener.onConnectivityChanged(IOnConnectivityChangedListener.Types.DOFSYSTEM_CREATE, dofSystem, "Secure system authorized");
        }
        else {
            //Log.d(SuitcaseDataStruct.LOG_TAG, "System is not authorized:"
            //                + "\nstate: " + state.toString()
            //);
            if (_onConnectivityChangedListener != null)
                _onConnectivityChangedListener.onConnectivityChanged(IOnConnectivityChangedListener.Types.FATAL_EXCEPTION, dofSystem, "System is not authorized: " + "\nstate: " + state.toString());
        }
    }

    @Override
    public void removed(DOFSystem dofSystem, DOFException e)
    {

    }

    // ************* DOFConnection.StateListener *************
    @Override
    public void stateChanged(DOFConnection dofConnection, State state)
    {
    	//REVIEW: Call onConnected?
        if(state.isConnected()) {
            if (_onConnectivityChangedListener != null)
                _onConnectivityChangedListener.onConnectivityChanged(IOnConnectivityChangedListener.Types.CONNECT, _dofSystem, "Suitcase connected!");

            // create secured _dofSystem
            try {
                if(_dofSystem == null) {
                    _dofSystem = DOFUtilities.createSecuredDOFSystem("SuitcaseProviderModel", _dofCredentials, _dof);
                    _dofSystem.addStateListener(this);
                }
            }
            catch (Exception e) {
                if(_onConnectivityChangedListener != null)
                    _onConnectivityChangedListener.onConnectivityChanged(IOnConnectivityChangedListener.Types.FATAL_EXCEPTION, null, "Exception thrown when creating DOFSystem: " + e.getMessage());
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Exception thrown when creating DOFSystem: " + e.getMessage());
            }

        } else {
            if (_onConnectivityChangedListener != null)
                _onConnectivityChangedListener.onConnectivityChanged(IOnConnectivityChangedListener.Types.DISCONNECT, _dofSystem, "Suitcase disconnected!");

//            if(state.getException() != null)
//            {
//                Log.d(SuitcaseDataStruct.LOG_TAG, "Connection disconnected"
//                                + "\nstate: " + state.toString()
//                                +  "\nexception: " + state.getException().getMessage()
//                                +  "\nexception: " + state.getTrafficStats().toString()
//                );
//            }
//            else
//            {
//                Log.d(SuitcaseDataStruct.LOG_TAG, "Connection disconnected"
//                                + "\nstate: " + state.toString()
//                                +  "\nexception: null"
//                                +  "\nexception: " + state.getTrafficStats().toString()
//                );
//            }

        }

    }

    @Override
    public void removed(DOFConnection dofConnection, DOFException e)
    {

    }

    // ************** Service **************
    @Override
    public void onCreate()
    {
        // Set default values for the connection.
        // this is only called once
        _instance = this;
        //REVIEW: Make log level a static field that can be easily seen/set at the top of the class
        DOF.Log.addListener(DOF.Log.Level.INFO, this);

        _dof = DOFUtilities.createDOF();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        //Log.d(SuitcaseDataStruct.LOG_TAG, "started");
        return super.onStartCommand(intent, flags, startId);
    }

    public void destroyConnection()
    {
        if (_reconnectingStateListener != null) {
            _reconnectingStateListener.cancel();
            _reconnectingStateListener = null;
        }

        // destroyConnection existing connection
        if(_dofConnection != null) {
            _dofConnection.destroy();
            _dofConnection = null;
        }

        // destroyConnection existing _dofSystem
        if(_dofSystem != null) {
            _dofSystem.destroy();
            _dofSystem = null;
        }

    }

    @Override
    public void onDestroy()
    {
        destroyConnection();

        // destroyConnection existing connection
        if(_dof != null) {
            _dof.destroy();
            _dof = null;
        }

        // destroy connectivity listener
        if (_onConnectivityChangedListener != null)
            _onConnectivityChangedListener = null;


        //Log.d("SuitcaseService", "destroyed");

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent i)
    {
        return null;
    }

    // ************* DOF.Log.Listener *************
    /**
     * Logs DOF mesages
     * @param time
     * @param level
     * @param message
     * @param throwable
     */
    @Override
    public void logMessage(Date time, Level level, String message, Throwable throwable)
    {
        switch (level) {
            case FATAL:
            case ERROR:
                Log.e("OpenDOF", time + " - " + level + " - " + message, throwable);
                break;
            case WARN:
                Log.w("OpenDOF", time + " - " + level + " - " + message, throwable);
                break;
            case INFO:
                Log.i("OpenDOF", time + " - " + level + " - " + message, throwable);
                break;
            case DEBUG:
                Log.d("OpenDOF", time + " - " + level + " - " + message, throwable);
                break;
            case TRACE:
                Log.v("OpenDOF", time + " - " + level + " - " + message, throwable);
                break;
        }
    }
}
