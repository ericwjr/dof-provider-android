package opendof.demo.suitcase.android.ProviderModel;

public interface ISuitcaseProviderDemoMBean
{
    /***
     * Lockable Interface
     */
    void lock();
    void unlock();
    boolean isLocked();

    /***
     *GPSLocation Interface
     */
    void setLocation(double longitude, double latitude, double elevation);
    void setLocationUnknown();
    double getLongitude();
    double getLatitude();
    double getElevation();

    /***
     * ImpactInterface
     */
    void setThreshold(double threshold);
    double getThreshold();
    void impactEvent(float x, float y, float z) throws Exception;
    void impactEvent(float dx, float dy, float dz, double magnitude) throws Exception;

    /***
     * WeightInterface
     */
    void setWeight(double weight);
    double getWeight();
}
