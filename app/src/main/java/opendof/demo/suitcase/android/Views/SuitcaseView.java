package opendof.demo.suitcase.android.Views;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.panasonic.monov.cstdemo.R;

import opendof.demo.suitcase.android.ProviderModel.SuitcaseDataStruct;

/**
 * Created by zdusatko on 2/19/2015.
 */
public class SuitcaseView extends LinearLayout implements Button.OnClickListener
{
    private Button _increaseWeightButton;
    private Button _decreaseWeightButton;
    private TextView _weightView;
    private Button _lockButton;
    private Button _unlockButton;
    private ImageView _padlockImageView;
    private TextView _oid;

    public TextView locationView;
    public ImageView statusView;

    private IOnDataInputListener _onDataInputListener = null;
    public void setOnDataInputListener(IOnDataInputListener onDataInputListener)
    {
        _onDataInputListener = onDataInputListener;
    }

    public SuitcaseView(Context context)
    {
        super(context);

        inflate(context, R.layout.activity_suitcase, this);

        statusView = (ImageView) findViewById(R.id.status);

        _oid = (TextView) findViewById(R.id.oid);

        _decreaseWeightButton = (Button) findViewById(R.id.decrease_weight);
        _increaseWeightButton = (Button) findViewById(R.id.increase_weight);
        _weightView = (TextView) findViewById(R.id.weight);

        _lockButton = (Button) findViewById(R.id.lock);
        _unlockButton = (Button) findViewById(R.id.unlock);

        _padlockImageView = (ImageView) findViewById(R.id.padlock);
        _padlockImageView.setImageResource(R.drawable.padlock_open);

        locationView = (TextView) findViewById(R.id.location);

        _decreaseWeightButton.setOnClickListener(this);
        _increaseWeightButton.setOnClickListener(this);
        _lockButton.setOnClickListener(this);
        _unlockButton.setOnClickListener(this);
    }

    public void updateOID(String oidString)
    {
        String suitcaseName = oidString;

        if(suitcaseName != null) {
            // if email format strip all after @ character included
            if (oidString.indexOf("@") != -1) {
                String[] nameArray = oidString.split("@");
                suitcaseName = nameArray[0];
            }

            // remove any occurrences of "..:"
            suitcaseName = suitcaseName.replaceAll("..:", "");
            suitcaseName = suitcaseName.replaceAll(".:", "");

            // remove any occurrences of "[" or "]"
            suitcaseName = suitcaseName.replace("[", "");
            suitcaseName = suitcaseName.replace("]", "");

            // remove any occurrences of "{" or "}"
            suitcaseName = suitcaseName.replace("{", "");
            suitcaseName = suitcaseName.replace("}", "");
        }

        _oid.setText(suitcaseName);
    }

    public void updateWeight(double weight)
    {
        if (weight > 0)
            _decreaseWeightButton.setEnabled(true);
        else
            _decreaseWeightButton.setEnabled(false);

        _weightView.setText(Double.toString(weight));
    }

    public void updateLocked(boolean isLocked)
    {
        if (isLocked) {
            _padlockImageView.setImageResource(R.drawable.padlock_closed);
            _lockButton.setEnabled(false);
            _unlockButton.setEnabled(true);
        }
        else {
            _padlockImageView.setImageResource(R.drawable.padlock_open);
            _lockButton.setEnabled(true);
            _unlockButton.setEnabled(false);
        }
    }

    public void updateConnectionIcon(SuitcaseDataStruct.SuitcaseState state)
    {
        switch (state) {
            case RED:
                statusView.setImageResource(R.drawable.red_light);
                break;
            case YELLOW:
                statusView.setImageResource(R.drawable.yellow_light);
                break;
            case GREEN:
                statusView.setImageResource(R.drawable.green_light);
                break;
        }
    }

    public void updateLocation(double latitude, double longitude, double elevation)
    {
        locationView.setText(String.format("%.3f %.3f %.1fm", latitude, longitude, elevation));
    }

    @Override
    public void onClick(View v)
    {
        int buttonId = v.getId();

        if (_onDataInputListener != null)
            _onDataInputListener.OnDataInput(buttonId);
    }
}
