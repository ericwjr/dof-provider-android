package opendof.demo.suitcase.android.TempCredentials;

/**
 * Created by zdusatko on 7/31/2015.
 */
public interface IOnCredentialsChangedListener
{
    public enum Types
    {
        ACQUIRE,
        EXCEPTION
    }

    void onCredentialsChanged(Types types, DOFObjectInitializationInformation objectInitializationInformation, Exception exception);
}
