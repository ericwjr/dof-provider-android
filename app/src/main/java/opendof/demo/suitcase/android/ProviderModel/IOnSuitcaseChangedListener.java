package opendof.demo.suitcase.android.ProviderModel;

/**
 * <h1>Listener for suitcase provider changes</h1>
 * <p></p>.
 *
 * @author zdusatko
 * @since 7/31/2015
 */
public interface IOnSuitcaseChangedListener
{
    public enum Types
    {
        WEIGHT,
        LOCK,
        GPS,
        IMPACT,
        THRESHOLD,
        PROVIDE,
        EXCEPTION
    }

    void onSuitcaseChanged(Types types, opendof.demo.suitcase.android.ProviderModel.SuitcaseDataStruct suitcaseDataStruct);
}
