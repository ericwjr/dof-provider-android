package opendof.demo.suitcase.android.TempCredentials;

import android.util.Log;

import org.opendof.core.oal.DOF;
import org.opendof.core.oal.DOFException;
import org.opendof.core.oal.DOFInterestLevel;
import org.opendof.core.oal.DOFInterfaceID;
import org.opendof.core.oal.DOFObject;
import org.opendof.core.oal.DOFObjectID;
import org.opendof.core.oal.DOFOperation;
import org.opendof.core.oal.DOFQuery;
import org.opendof.core.oal.DOFResult;
import org.opendof.core.oal.DOFSystem;
import org.opendof.core.oal.DOFType;
import org.opendof.core.oal.DOFValue;
import org.opendof.core.oal.security.DOFPermission;
import org.opendof.core.oal.security.DOFPermissionSet;
import opendof.demo.suitcase.android.ProviderModel.SuitcaseDataStruct;
import opendof.demo.suitcase.android.Utilities.DOFUtilities;
import opendof.demo.suitcase.common.TempPasswordCredentialsInterface;

import java.util.List;

/**
 * Created by zdusatko on 2/27/2015.
 */
/**
 * <h1>Listener for connectivity changes</h1>
 * <p></p>.
 *
 * @author Jonathan Stringer
 * @since 2/20/2015
 */
public class CredentialsRequestor implements DOFSystem.QueryOperationListener
{
    private Object lock = new Object();

    String _uniqueID;
    DOFSystem _system = null;

    private IOnCredentialsChangedListener _onCredentialsChangedListener = null;
    public void setOnCredentialsChangedListener(IOnCredentialsChangedListener onCredentialsChangedListener)
    {
        _onCredentialsChangedListener = onCredentialsChangedListener;
    }

    // Singleton
    private static CredentialsRequestor _instance = null;

    public static CredentialsRequestor getInstance()
    {
        if (_instance == null) {
            synchronized (CredentialsRequestor.class) {
                _instance = new CredentialsRequestor();
            }
        }

        return _instance;
    }

    // CONSTRUCTOR
    private CredentialsRequestor()
    {

    }

    public void init(String uniqueID, DOFSystem system)
    {
        _uniqueID = uniqueID;
        _system = system;
        //Thread.sleep(10000);
        try {
            DOFOperation.Interest interest = system.beginInterest(
                    DOFObjectID.BROADCAST, TempPasswordCredentialsInterface.DEF.getInterfaceID(), DOFInterestLevel.WATCH, DOF.TIMEOUT_NEVER, new DOFSystem.InterestOperationListener()
                    {
                        @Override
                        public void complete(DOFOperation dofOperation, DOFException e)
                        {
//                            Log.d(SuitcaseDataStruct.LOG_TAG, dofOperation.toString());
//                            if(e != null)
//                            Log.d(SuitcaseDataStruct.LOG_TAG, "beginInterest exception: " + e.getMessage());
//                            else
//                            Log.d(SuitcaseDataStruct.LOG_TAG, "beginInterest completed normally");
                        }
                    });

            DOFQuery query = new DOFQuery.Builder()
                    .addFilter(TempPasswordCredentialsInterface.DEF.getInterfaceID())
                    .build();
            DOFOperation.Query queryOp = _system.beginQuery(query, DOF.TIMEOUT_NEVER, this, null);
        }
        catch (Exception e) {
            if(_onCredentialsChangedListener != null)
                _onCredentialsChangedListener.onCredentialsChanged(IOnCredentialsChangedListener.Types.EXCEPTION, null, e);
        }
    }

    public DOFObjectInitializationInformation requestCredentials(DOFObjectID uniqueID, DOFObject provider) throws Exception
    {
        try {
            DOFResult<List<DOFValue>> results = provider.invoke(TempPasswordCredentialsInterface.DEF.getMethod(1), DOFUtilities.TIMEOUT, uniqueID);

            DOFObjectInitializationInformation objectInfo = new DOFObjectInitializationInformation();
            List<DOFValue> values = results.get();
            objectInfo.setIdentity(DOFObjectID.Authentication.create(DOFType.asDOFObjectID(values.get(0))));
            objectInfo.setPassword(DOFType.asString(values.get(1)));
            objectInfo.setDomain(DOFObjectID.Domain.create(provider.getObjectID().getBase()));

            DOFPermissionSet permissions = new DOFPermissionSet.Builder(DOFType.asBytes(values.get(2))).build();
            for (DOFPermission permission : permissions) {
                if (permission.getPermissionType() == DOFPermission.BINDING) {
                    DOFPermission.Binding bindingPermission = (DOFPermission.Binding) permission;
                    List<DOFObjectID> oids = bindingPermission.getObjectIDs();
                    List<DOFInterfaceID> iids = bindingPermission.getInterfaceIDs();

                    if (oids.isEmpty()) {
                        if (iids.isEmpty()) {
                            objectInfo.addBinding(DOFObjectID.BROADCAST, DOFInterfaceID.WILDCARD);
                        }
                        else {
                            for (DOFInterfaceID iid : iids) {
                                objectInfo.addBinding(DOFObjectID.BROADCAST, iid);
                            }
                        }
                    }
                    else {
                        for (DOFObjectID oid : oids) {
                            if (iids.isEmpty()) {
                                objectInfo.addBinding(oid, DOFInterfaceID.WILDCARD);
                            }
                            else {
                                for (DOFInterfaceID iid : iids) {
                                    objectInfo.addBinding(oid, iid);
                                }
                            }
                        }
                    }
                }
            }

            return objectInfo;
        }
        catch (DOFException e) {
            throw e;
        }
    }

    // **************** DOFSystem.QueryOperationListener *******************
    @Override
    public void interfaceAdded(DOFOperation.Query query, DOFObjectID dofObjectID, DOFInterfaceID dofInterfaceID)
    {
        synchronized (lock) {
            try {
                DOFObject dofObjectProvider = _system.createObject(dofObjectID);
                DOFObjectInitializationInformation objectInfo = requestCredentials(DOFObjectID.create(_uniqueID), dofObjectProvider);

                if (objectInfo != null) {
                    objectInfo.setDomain(DOFObjectID.Domain.create(dofObjectProvider.getObjectID().getBase()));
                    //Log.d(SuitcaseDataStruct.LOG_TAG, objectInfo.toString());
                }
                //else
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Unable to get temporary credentials.");

                if(_onCredentialsChangedListener != null)
                    _onCredentialsChangedListener.onCredentialsChanged(IOnCredentialsChangedListener.Types.ACQUIRE, objectInfo, null);
                //else
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Credentials acquired listener not setup");
            }
            catch (Exception e) {
                //Log.d(SuitcaseDataStruct.LOG_TAG, "Exception when getting temporary credentials: " + e.getMessage());
                if(_onCredentialsChangedListener != null)
                    _onCredentialsChangedListener.onCredentialsChanged(IOnCredentialsChangedListener.Types.EXCEPTION, null, e);
            }
        }
    }

    @Override
    public void interfaceRemoved(DOFOperation.Query query, DOFObjectID dofObjectID, DOFInterfaceID dofInterfaceID)
    {

    }

    @Override
    public void providerRemoved(DOFOperation.Query query, DOFObjectID dofObjectID)
    {

    }

    @Override
    public void complete(DOFOperation dofOperation, DOFException e)
    {

    }
}