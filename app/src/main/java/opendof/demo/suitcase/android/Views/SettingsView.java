package opendof.demo.suitcase.android.Views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.panasonic.monov.cstdemo.R;

/**
 * Created by zdusatko on 2/23/2015.
 */
public class SettingsView extends LinearLayout
{
    private EditText _hostBox;
    private EditText _portBox;
    private EditText _oidBox;
    private EditText _domainBox;
    private EditText _identityBox;
    private EditText _passwordBox;
    private AlertDialog _dialog;

    public Button scannerButton;

    private IOnOKClickedListener _onOKClickedListener = null;
    public void setOnOKClickedListener(IOnOKClickedListener onOKClickListener)
    {
        _onOKClickedListener = onOKClickListener;
    }

    // CONSTRUCTOR
    public SettingsView(Context context)
    {
        super(context);

        inflate(context, R.layout.settings_view, this);

        scannerButton = (Button)findViewById(R.id.scan_button);

        _hostBox  = (EditText) findViewById(R.id.host);
        _hostBox.setSelectAllOnFocus(true);
        _hostBox.setSelected(true);
        _hostBox.setImeOptions(EditorInfo.IME_ACTION_SEND);

        _portBox = (EditText) findViewById(R.id.port);
        _portBox.setSelectAllOnFocus(true);
        _portBox.setInputType(InputType.TYPE_CLASS_NUMBER);
        _portBox.setImeOptions(EditorInfo.IME_ACTION_SEND);

        _oidBox = (EditText) findViewById(R.id.oid);
        _oidBox.setSelectAllOnFocus(true);
        _oidBox.setImeOptions(EditorInfo.IME_ACTION_SEND);

        _domainBox = (EditText) findViewById(R.id.domain);
        _domainBox.setSelectAllOnFocus(true);
        _domainBox.setImeOptions(EditorInfo.IME_ACTION_SEND);

        _identityBox = (EditText) findViewById(R.id.identity);
        _identityBox.setSelectAllOnFocus(true);
        _identityBox.setImeOptions(EditorInfo.IME_ACTION_SEND);

        _passwordBox = (EditText) findViewById(R.id.password);
        _passwordBox.setSelectAllOnFocus(true);
        _passwordBox.setImeOptions(EditorInfo.IME_ACTION_SEND);

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        dialogBuilder.setView(this);
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int whichButton)
            {
                String host = _hostBox.getText().toString();
                int port = Integer.parseInt(_portBox.getText().toString());
                String suitcaseID = _oidBox.getText().toString();
                String domain = _domainBox.getText().toString();
                String identity = _identityBox.getText().toString();
                String password = _passwordBox.getText().toString();

                if (_onOKClickedListener != null)
                    _onOKClickedListener.onOKClicked(host, port, suitcaseID, domain, identity, password);

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.cancel();
            }
        });
        dialogBuilder.setTitle("Settings");


        _dialog = dialogBuilder.create();
        _dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        _dialog.show();
    }

    public void enableOKButton()
    {
        _dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
    }

    public void disableOKButton()
    {
        _dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
    }

    public void updateViewCredentials(String host, int port, String oid, String domain, String identity, String password)
    {
        // host
        _hostBox.setText(host);
        // port
        _portBox.setText(Integer.toString(port));
        // oid
        _oidBox.setText(oid);
        // domainID
        _domainBox.setText(domain);
        // identity
        _identityBox.setText(identity);
        // password
        _passwordBox.setText(password);
    }
}
