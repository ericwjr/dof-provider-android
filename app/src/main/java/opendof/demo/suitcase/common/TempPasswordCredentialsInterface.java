/*
 * This work is protected by Copyright, see COPYING.txt for more information.
 */
package opendof.demo.suitcase.common;

import org.opendof.core.oal.DOFInterface;
import org.opendof.core.oal.DOFInterfaceID;
import org.opendof.core.oal.DOFObjectID;
import org.opendof.core.oal.DOFType;
import org.opendof.core.oal.value.DOFBlob;
import org.opendof.core.oal.value.DOFDateTime;
import org.opendof.core.oal.value.DOFString;

/**
 * IID converted from code-gen to enum for type safety and convenience.
 */
public class TempPasswordCredentialsInterface
{
    public static final DOFInterfaceID IID = DOFInterfaceID.create("[1:{01000054}]");
    private static final DOFInterface.Builder ibuilder = new DOFInterface.Builder(IID);
    public static final DOFInterface DEF;

    public enum TYPE
    {
        Identity(DOFObjectID.TYPE),
        Password(new DOFString.Type(3, 32)),
        PermissionSet(new DOFBlob.Type(1, 4096)),
        Expiry(DOFDateTime.TYPE);

        TYPE(DOFType doftype)
        {
            T = doftype;
        }

        public final DOFType T;
    }

    static {
        ibuilder.addMethod(1, new DOFType[]{TYPE.Identity.T}, new DOFType[]{TYPE.Identity.T, TYPE.Password.T, TYPE.PermissionSet.T, TYPE.Expiry.T}); //SetStatus
        ibuilder.addException(2, new DOFType[]{}); //StatusActivateFailed
        DEF = ibuilder.build();
    }

    public enum METHOD
    { //convenient handle for the programmer
        RequestCredentials(DEF.getMethod(1));

        METHOD(DOFInterface.Method m)
        {
            M = m;
            ID = m.getItemID();
        }

        ;
        public final int ID;
        private DOFInterface.Method M;

        public DOFInterface.Method get()
        {
            return M;
        }
    }

    public enum EXCEPTION
    { //convenient handle for the programmer
        Failed(DEF.getException(2));

        EXCEPTION(DOFInterface.Exception x)
        {
            X = x;
            ID = x.getItemID();
        }

        ;
        public final int ID;
        private DOFInterface.Exception X;

        public DOFInterface.Exception get()
        {
            return X;
        }
    }

}
