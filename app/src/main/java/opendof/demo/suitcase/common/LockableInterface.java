package opendof.demo.suitcase.common;

import org.opendof.core.oal.DOFInterface;
import org.opendof.core.oal.DOFInterfaceID;
import org.opendof.core.oal.DOFType;
import org.opendof.core.oal.value.DOFBoolean;

public class LockableInterface {
    //>>> Generated code
    /* Java Language Information for Lockable generated on Fri Dec 20 11:14:45 MST 2013 */
    
    /*
      Types
        LockState: boolean
          LockState - The lock state of the item.  True means locked and
          false means unlocked.
    
      Properties
        isLocked: LockState
          isLocked - This is the current lock state of the item.
    
      Methods
        lock: () -> ()
          lock - Lock the item.  This may fail if not currently lockable.
           In that case, the Failed exception is thrown.
    
        unlock: () -> ()
          unlock - Unlock the item.  This may fail if the item is not
          currently unlockable.  In that case, the Failed exception is
          thrown.
    
      Events
      Exceptions
        failed: ()
          failed - This exception may occur when the item cannot be
          locked or unlocked.
    
    */
    
    static DOFType LockState = DOFBoolean.TYPE;
    static DOFInterfaceID Lockable_iid = DOFInterfaceID.create( "[1:{0100004B}]" );
    static DOFInterface Lockable_def = new DOFInterface.Builder( Lockable_iid )
        .addProperty( 3, false, true, LockState )
        .addMethod( 1, new DOFType[] { }, new DOFType[] { } )
        .addMethod( 2, new DOFType[] { }, new DOFType[] { } )
        .addException( 4, new DOFType[] { } )
        .build();
    //<<< Generated code
    
    public static final DOFInterfaceID iid = Lockable_iid;
    public static final DOFInterface iface = Lockable_def;

    public static final int PROPERTY_ISLOCKED = 3;
    public static final int METHOD_LOCK = 1;
    public static final int METHOD_UNLOCK = 2;
    public static final int EXCEPTION_FAILED = 4;

    public static final DOFInterface.Property propertyIsLocked = iface.getProperty(PROPERTY_ISLOCKED);
    public static final DOFInterface.Method methodLock = iface.getMethod(METHOD_LOCK);
    public static final DOFInterface.Method methodUnlock = iface.getMethod(METHOD_UNLOCK);
    public static final DOFInterface.Exception exceptionFailed = iface.getException(EXCEPTION_FAILED);
}
