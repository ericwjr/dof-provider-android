package opendof.demo.suitcase.common;

import org.opendof.core.oal.DOFInterface;
import org.opendof.core.oal.DOFInterfaceID;
import org.opendof.core.oal.DOFType;
import org.opendof.core.oal.value.DOFFloat64;

public class WeightInterface {
    //>>> Generated code
    /* Java Language Information for Weight generated on Fri Dec 20 11:16:22 MST 2013 */
    
    /*
      Types
        Weight: float64
          Weight - The physical weight of the object.
    
      Properties
        weight: Weight
          weight - The current weight of the object.
    
      Methods
      Events
      Exceptions
    */
    
    static DOFType Weight = DOFFloat64.TYPE;
    static DOFInterfaceID Weight_iid = DOFInterfaceID.create( "[1:{0100004C}]" );
    static DOFInterface Weight_def = new DOFInterface.Builder( Weight_iid )
        .addProperty( 1, true, true, Weight )
        .build();
    //<<< Generated code

    public static final DOFInterfaceID iid = Weight_iid;
    public static final DOFInterface iface = Weight_def;

    public static final int PROPERTY_WEIGHT = 1;

    public static final DOFInterface.Property propertyWeight = iface.getProperty(PROPERTY_WEIGHT);
}
