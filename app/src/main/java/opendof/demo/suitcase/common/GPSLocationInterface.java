package opendof.demo.suitcase.common;

import org.opendof.core.oal.DOFInterface;
import org.opendof.core.oal.DOFInterfaceID;
import org.opendof.core.oal.DOFType;
import org.opendof.core.oal.value.DOFFloat64;
import org.opendof.core.oal.value.DOFStructure;

public class GPSLocationInterface {
    //>>> Generated code
    /* Java Language Information for GPS Location generated on Fri Dec 20 11:05:01 MST 2013 */
    
    /*
      Types
        Latitude: float64
          Latitude - The latitude component of the GPS location.
    
        Longitude: float64
          Longitude - The longitude component of the GPS location.
    
        Elevation: float64
          Elevation - The elevation component of the GPS location.
    
        Location: structure{ Latitude, Longitude, Elevation, }
          Location - The location consisting of latitude, longitude, and
          elevation using the WGS84 datum.
    
      Properties
        location: Location
          location - The GPS location.
    
      Methods
      Events
      Exceptions
    */
    
    public static DOFType Latitude = DOFFloat64.TYPE;
    public static DOFType Longitude = DOFFloat64.TYPE;
    public static DOFType Elevation = DOFFloat64.TYPE;
    public static DOFStructure.Type Location = new DOFStructure.Type( new DOFType[] { Latitude, Longitude, Elevation,  } );
    public static DOFInterfaceID GPSLocation_iid = DOFInterfaceID.create("[1:{01000048}]");
    public static DOFInterface GPSLocation_def = new DOFInterface.Builder( GPSLocation_iid )
        .addProperty(1, false, true, Location)
        .build();
    //<<< Generated code

    public static final DOFInterfaceID iid = GPSLocation_iid;
    public static final DOFInterface iface = GPSLocation_def;

    public static final int PROPERTY_LOCATION = 1;
    
    public static final DOFInterface.Property propertyLocation = iface.getProperty(PROPERTY_LOCATION);
}
