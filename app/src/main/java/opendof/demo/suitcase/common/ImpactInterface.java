package opendof.demo.suitcase.common;

import org.opendof.core.oal.DOFInterface;
import org.opendof.core.oal.DOFInterfaceID;
import org.opendof.core.oal.DOFType;
import org.opendof.core.oal.value.DOFFloat32;
import org.opendof.core.oal.value.DOFFloat64;
import org.opendof.core.oal.value.DOFStructure;

public class ImpactInterface {
    //>>> Generated code
    /* Java Language Information for Impact generated on Fri Dec 20 11:10:20 MST 2013 */
    
    /*
      Types
        Direction: float32
          Direction - The normalized length of the force vector along the
          specific axis.
    
        Magnitude: float64
          Magnitude - The length of the force vector.
    
        Force: structure{ Direction, Direction, Direction, Magnitude, }
          Force - The force vector, which contains the direction
          component and the magnitude component.  The direction is the
          3-tuple (i.e., {x, y, z}) of a Euclidian unit vector.
    
        Latitude: float64
          Latitude - The latitude component of the GPS location.
    
        Longitude: float64
          Longitude - The longitude component of the GPS location.
    
        Elevation: float64
          Elevation - The elevation component of the GPS location.
    
        Location: structure{ Latitude, Longitude, Elevation, }
          Location - The location consisting of latitude, longitude, and
          elevation using the WGS84 datum.
    
        NullableLocation: nullable(Location);
          NullableLocation - A nullable GPS location.
    
      Properties
        threshold: Magnitude
          threshold - The threshold for triggered events.  Any impact
          that occurs with a force magnitude greater than this threshold,
          will trigger an 'impact' event.
    
      Methods
      Events
        impact: (NullableLocation, Force, )      impact - An impact has occurred.  The parameters include the
          date/time and location of the event as well as the force along
          each axis.
    
      Exceptions
    */
    
    public static DOFType Direction = DOFFloat32.TYPE;
    public static DOFType Magnitude = DOFFloat64.TYPE;
    public static DOFStructure.Type Force = new DOFStructure.Type( new DOFType[] { Direction, Direction, Direction, Magnitude,  } );
    public static DOFType Latitude = DOFFloat64.TYPE;
    public static DOFType Longitude = DOFFloat64.TYPE;
    public static DOFType Elevation = DOFFloat64.TYPE;
    public static DOFStructure.Type Location = new DOFStructure.Type( new DOFType[] { Latitude, Longitude, Elevation,  } );
    public static DOFType NullableLocation = new DOFType.Nullable(Location);
    public static DOFInterfaceID Impact_iid = DOFInterfaceID.create( "[1:{01000049}]" );
    public static DOFInterface Impact_def = new DOFInterface.Builder( Impact_iid )
        .addProperty( 2, true, true, Magnitude )
        .addEvent( 1, new DOFType[] { NullableLocation, Force, } )
        .build();
    //<<< Generated code

    public static final DOFInterfaceID iid = Impact_iid;
    public static final DOFInterface iface = Impact_def;

    public static final int EVENT_IMPACT = 1;
    public static final int PROPERTY_THRESHOLD = 2;

    public static final DOFInterface.Event eventImpact = iface.getEvent(EVENT_IMPACT);
    public static final DOFInterface.Property propertyThreshold = iface.getProperty(PROPERTY_THRESHOLD);
}
